%include "lib.inc"
%include "words.inc"
%include "dict.inc"

%define MAX_LENGTH 255

section .rodata
	not_found_error: db 'Value not found', 0
	max_length_error: db 'Max avalliable length 255', 0

section .bss
	buffer: resb 256

section .text
	global _start

_start:
	mov rdi, buffer
	mov rsi, MAX_LENGTH
	call read_word

	test rax, rax
	je .length_error
	mov rdi, rax
	mov rsi, first_word
	call find_word
	test rax, rax
	jz .not_found

	mov rdi, rax
	push rdi
	call string_length
	pop rdi
	add rdi, rax
	inc rdi
	call print_string
	jmp .exit

.not_found:
	mov rdi, not_found_error
    call print_error

.exit:
	call exit

.length_error:
	mov rdi, max_length_error
	call print_error
	jmp .exit

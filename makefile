ASM=nasm
ASMFLAGS=-f elf64
LD=ld
PYTHON=python3.7


ASM_FILES=$(wildcard *.asm) 
INC_FILES=$(wildcard *.inc) 
OBJ_FILES=$(ASM_FILES:%.asm=%.o)


%.o: %.asm $(INC_FILES)
	$(ASM) $(ASMFLAGS) -o $@ $<

init: $(OBJ_FILES) 
	$(LD) -o $@ $^

.PHONY: clean test
clean:
	rm -rf *.o init

test:
	make clean
	make init
	$(PYTHON) test.py

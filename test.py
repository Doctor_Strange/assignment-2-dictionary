import subprocess 
import unittest

class TestDictionary(unittest.TestCase): 
    def run_program(self, stdin):
        program_path = "./init"  
        process = subprocess.Popen(
            [program_path],
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True,
            shell=True  
        )
        stdout, stderr = process.communicate(input=stdin)
        return stdout.strip(), stderr.strip()  
        
    def final_test(self, words):
        for stdin, expected_stdout, expected_stderr in words:
            with self.subTest(stdin=stdin):
                result_stdout, result_stderr = self.run_program(stdin)
                self.assertEqual(result_stdout, expected_stdout)
                self.assertEqual(result_stderr, expected_stderr)
        
    def test_valid_words(self):
        words = [
            ("aboba", "first", ""), 
            ("bebra", "second", "")
        ]
            
        self.final_test(words)
        
    def test_word_not_found(self):
        words = [
            ("minecraft", "", "Value not found")
        ]
        self.final_test(words)

    def test_max_length(self):
        words = [
            ("minecraft_minecraft_minecraft_minecraft_minecraft_minecraft_minecraft_minecraft_minecraft_minecraft_minecraft_minecraft_minecraft_minecraft_minecraft_minecraft_minecraft_minecraft_minecraft_minecraft_minecraft_minecraft_minecraft_minecraft_minecraft_minecraft_minecraft_minecraft_minecraft_minecraft_minecraft_minecraft_minecraft_minecraft_minecraft_minecraft_minecraft_minecraft_", "", "Max avalliable length 255")
        ]
        self.final_test(words)

if __name__ == "__main__":
    unittest.main()
